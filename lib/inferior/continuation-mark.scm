;; Copyright (C) Marc Nieper-Wißkirchen (2017).  All Rights Reserved.

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(define (make-mark-set shallow-keys frames) (vector shallow-keys frames))
(define (make-continuation-mark-set) (make-mark-set '() '()))
(define (mark-set-shallow-keys set) (vector-ref set 0))
(define (mark-set-frames set) (vector-ref set 1))

(define (remove-mark-in-frame frame key)
  (let loop ((frame frame))
    (cond
     ((null? frame)
      '())
     ((eq? key (caar frame))
      (cdr frame))
     (else
      (cons (car frame)
	    (loop (cdr frame)))))))

(define (set-mark-in-frame frame key value)
  (call-with-current-continuation
   (lambda (return)
     (let loop ((%frame frame))
       (cond
	((null? %frame)
	 (return (cons (cons key value) frame)))
	((eq? key (caar %frame))
	 (cons (cons key value)
	       (cdr %frame)))
	(else
	 (cons (car %frame)
	       (loop (cdr %frame)))))))))

(define (remove-shallow-marks set key)
  (let loop ((shallow-keys (mark-set-shallow-keys set)))
    (cond
     ((null? shallow-keys)
      (make-mark-set '() (mark-set-frames set)))
     ((eq? key (caar shallow-keys))
      (make-mark-set (cdr shallow-keys)
		     (let loop ((frames (mark-set-frames set)))
		       (cond
			((eq? (car frames) (cdar shallow-keys))
			 (cons (remove-mark-in-frame (car frames) key)
			       (cdr frames)))
			(else
			 (cons (car frames)
			       (loop (cdr frames))))))))
     (else
      (let ((set (loop (cdr shallow-keys))))
	(make-mark-set (cons (car shallow-keys)
			     (mark-set-shallow-keys set))
		       (mark-set-frames set)))))))

(define (update-frame shallow-keys frame new-frame)
  (map (lambda (assoc)
	 (cond
	  ((eq? (cdr assoc) frame)
	   (cons (car assoc) new-frame))
	  (else
	   assoc)))
       shallow-keys))

(define (continuation-mark-set flag set key value)
  (let* ((set (if (continuation-mark-shallow-key? key)
		  (remove-shallow-marks set key)
		  set))
	 (shallow-keys (mark-set-shallow-keys set))
	 (frames (mark-set-frames set))
	 (new-frames (if flag
			 (cons (set-mark-in-frame (car frames) key value)
			       (cdr frames))
			 (cons (list (cons key value))
			       frames)))
	 (shallow-keys (if flag
			     (update-frame shallow-keys
					   (car frames)
					   (car new-frames))
			     shallow-keys))
	 (shallow-keys (if (continuation-mark-shallow-key? key)
			     (cons (cons key (car new-frames)) shallow-keys)
			     shallow-keys)))
    (make-mark-set shallow-keys new-frames)))

(define (continuation-mark-set->list set key)
  (let loop ((frames (mark-set-frames set)))
    (cond
     ((null? frames)
      '())
     ((assq key (car frames))
      => (lambda (pair)
	   (cons (cdr pair)
		 (if (continuation-mark-shallow-key? key)
		     '()
		     (loop (cdr frames))))))
     (else
      (loop (cdr frames))))))

(define (any pred list)
  (let loop ((list list))
    (and (not (null? list))
	 (or (pred (car list))
	     (loop (cdr list))))))

(define (continuation-mark-set->list* set list . default*)
 (let ((default (and (not (null? default*)) (car default*))))
    (let loop ((frames (mark-set-frames set)))
      (cond
       ((null? frames)
	'())
       ((not (any (lambda (key)
		    (assq key (car frames)))
		  list))
	(loop (cdr frames)))
       (else
	(cons (list->vector (map (lambda (key)
				   (cond
				    ((assq key (car frames)) => cdr)
				    (else default)))
				 list))
	      (loop (cdr frames))))))))


(define (continuation-mark-set-first set key . default*)
  (let ((default (and (not (null? default*)) (car default*))))
    (let loop ((frames (mark-set-frames set)))
      (cond
       ((null? frames) default)
       ((assq key (car frames)) => cdr)
       (else (loop (cdr frames)))))))

(define-record-type <continuation-mark-key>
  (%make-continuation-mark-key sym)
  continuation-mark-key?
  (sym continuation-mark-key-symbol))

(define (make-continuation-mark-key)
  (%make-continuation-mark-key #f))

(define-record-type <continuation-mark-shallow-key>
  (%make-continuation-mark-shallow-key sym)
  continuation-mark-shallow-key?
  (sym continuation-mark-shallow-key-symbol))

(define (make-continuation-mark-shallow-key)
  (%make-continuation-mark-shallow-key #f))

(define (continuation-mark-immediate set key default)
  (cond
   ((assq key (car (mark-set-frames set))) => cdr)
   (else default)))

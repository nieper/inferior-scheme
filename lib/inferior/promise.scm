;; Copyright (C) Marc Nieper-Wißkirchen (2018).  All Rights Reserved.

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(define-record-type <promise>
  (%make-promise content)
  promise?
  (content promise-content promise-set-content!))

(define-record-type <promise-content>
  (make-promise-content done? value dynamic-extent)
  promise-content?
  (done? promise-content-done? promise-content-set-done!)
  (value promise-content-value promise-content-set-value!)
  (dynamic-extent promise-content-dynamic-extent promise-content-set-dynamic-extent!))

(define (promise-done? promise) (promise-content-done? (promise-content promise)))
(define (promise-set-done! promise done?)
  (promise-content-set-done! (promise-content promise) done?))
(define (promise-value promise) (promise-content-value (promise-content promise)))
(define (promise-set-value! promise value)
  (promise-content-set-value! (promise-content promise) value))
(define (promise-dynamic-extent promise)
  (promise-content-dynamic-extent (promise-content promise)))
(define (promise-set-dynamic-extent! promise dynamic-extent)
  (promise-content-set-dynamic-extent! (promise-content promise) dynamic-extent))

(define (make-promise done? value marks)
  (%make-promise (make-promise-content done? value marks)))

(define (promise-update! new old)
  (promise-set-done! old (promise-done? new))
  (promise-set-value! old (promise-value new))
  (promise-set-dynamic-extent! old (promise-dynamic-extent new))
  (promise-set-content! new (promise-content old)))


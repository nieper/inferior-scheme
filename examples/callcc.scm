(define (ccm)
  (continuation-mark-set-first (current-continuation-marks) 'key 'outer))

(define (cc)
  (call-with-current-continuation
   (lambda (c) c)))

(call-with-current-continuation
 (lambda (exit)
   (let ((c #f))
     (with-continuation-mark
      'key 'inner
      (let ((k (cc)))
	(display (ccm))
	(newline)
	(cond
	 (c
	  (exit 'done))
	 (else
	  (set! c k)))))
     (display (ccm))
     (newline)
     ((c c)))))

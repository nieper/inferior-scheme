(define (f x)
  x)

(define key (make-continuation-mark-shallow-key))

(define (ccm key)
  (continuation-mark-set->list (current-continuation-marks) key))

(define (ccm* keys)
  (continuation-mark-set->list* (current-continuation-marks) keys))

(define cde current-dynamic-extent)

(define (test name expected actual)
  (display name)
  (display ": ")
  (cond
   ((equal? expected actual)
    (display "PASS"))
   (else
    (display "FAIL")
    (newline)
    (display "  Expected: ")
    (display expected)
    (display "  Actual: ")
    (display actual)))
  (newline))

(test "Addition" 2 (+ 1 1))

(test "Continuation Marks I"
      '()
      (ccm 'key))

(test "Continuation Marks II"
      '(2)
      (with-continuation-mark 'key 1
			      (with-continuation-mark 'key 2
						      (ccm 'key))))

(test "Continuation Marks III"
      '(2 1)
      (with-continuation-mark 'key 1
			      (f (with-continuation-mark 'key 2
							 (ccm 'key)))))

(test "Continuation Marks IV"
      '(2)
      (with-continuation-mark key 1
			      (f (with-continuation-mark key 2
							 (ccm key)))))

(test "Continuation Marks V"
      '1
      (with-continuation-mark
       'key 1
       (call-with-immediate-continuation-mark
	'key
	(lambda (m)
	  m))))

(test "Dynamic Extents"
      '(#(2 b) #(1 #f))
      (with-dynamic-extent
       (with-continuation-mark
	'key 1
	(with-continuation-mark
	 key 'a
	 (f (with-continuation-mark
	     'key 2
	     (with-continuation-mark
	      key 'b
	      (cde))))))
       (lambda ()
	 (ccm* (list 'key key)))))

(test "Promises I"
      'value
      (let ((promise (delay
		       (with-dynamic-extent
			(forcing-extent)
			(lambda ()
			  (call-with-immediate-continuation-mark
			   'key
			   (lambda (value) value)))))))
	(with-continuation-mark
	 'key 'value
	 (force
	  (delay
	    (with-continuation-mark
	     'key 'foo
	     (force
	      (delay (force promise)))))))))

(test "Promises II"
      '(original-OK 42)
      (let* ((outer-E #f)
	     (res1 (force
		    (delay
		      (begin
			(set! outer-E (current-dynamic-extent))
			'original-OK))))
	     (res2 (force
		    (delay
		      (with-dynamic-extent outer-E (lambda ()
						     (force (delay 42))))))))
	(list res1 res2)))
